-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 07, 2016 at 11:57 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.6.22-1+donate.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dev.admin.honed`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(150) NOT NULL,
  `password` varchar(128) NOT NULL,
  `password_last_update` datetime NOT NULL,
  `type` int(2) NOT NULL,
  `salt` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `middle_name`, `email`, `phone`, `password`, `password_last_update`, `type`, `salt`, `image`, `created`, `modified`) VALUES
(2, 'Joshua', 'Deloso', 'Pandaan', 'asd@yahoo.com1', '', '99WIKHSsNBmGg', '0000-00-00 00:00:00', 0, '2defae18474e6fb329fa09df821ba8db8a68d108', '2.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '', '', '', 'run@run.run', '', '99WIKHSsNBmGg', '2016-07-07 14:26:26', 0, '2defae18474e6fb329fa09df821ba8db8a68d108', 'None', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
