<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;


use Cake\Controller\Controller;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use App\Model\Table\App\Model\Table;

class BackendController extends AppController
{
    
    public function initialize(){
        parent::initialize();        
        $this->viewBuilder()->layout("admin_v1") ;
        $user_id = ($this->request->session()->check("user_token")?$this->request->session()->read("user_token"):0);
        if (!$user_id){
                $this->redirect(array(
                                "controller" => "users",
                                "action" => "login"
                ));
        }else{        
            return $user_id;
        }
    }
    
    public function index(){
//        echo "Sesniel";
    }
    
    public function xml(){
        $UserTable    = TableRegistry::get('User');  
        $JobTable     = TableRegistry::get('Jobs');
        $WagerTable   = TableRegistry::get('Wagers');
        $EntryTable   = TableRegistry::get('Entries');
        $errorCode    = array();
        $xml = simplexml_load_string($this->request->data['xml_data'], "SimpleXMLElement", LIBXML_NOCDATA);

        $token    = $this->request->data['token'];   //$xml->attributes()->token;
        $username = $this->request->data['username'];//$xml->attributes()->username;
        $password = $this->request->data['password'];//$xml->attributes()->password;

        $userDetails = $UserTable->find('all')
                        ->where([
                            'User.password'    => crypt($password,99),
                            'User.username' => $username,
                            'User.token'    => $token                           
                        ])
                        ->first(); 
        
        
        if(!empty($userDetails)):

            $job_id = "CMS_" . sha1(rand());
            $jobDetails = $JobTable->newEntity();
            $jobDetails->user_id      = $userDetails->id;
            $jobDetails->file_content = $this->request->data['xml_data'];
            $jobDetails->status       = "Received";
            $jobDetails->job_id       = $job_id;
            $saveJob = $JobTable->save($jobDetails);
            $wagerNumber = 0;
            $entryNumber = 0;
            
            if($saveJob): 
                
                foreach($xml->wager as $jobs): 
                    $wagerNumber++;
                    $wager_id     = $jobs->attributes()->id;
                    $product_id   = $jobs->attributes()->product_type;
                    $draw_date    = $jobs->attributes()->draw_date;
                    $draw_number  = $jobs->attributes()->draw_number;                
                    $wagerDetails = $WagerTable->newEntity();
                    
                    
                    
                    
                    $wagerDetails->user_id      = $userDetails->id;
                    $wagerDetails->detail_full = $wager_id;
                    $wagerDetails->job_id       = $saveJob->id;
                    $wagerDetails->product_id   = $product_id;
                    $wagerDetails->draw_date    = $draw_date;
                    $wagerDetails->draw_number  = $draw_number;
                    $wagerDetails->status       = "Received";
                    
//                    echo $jobs->attributes()->id;
                    
                    $wagerSave = $WagerTable->save($wagerDetails);
                    
        
//                    exit;
                    if($wagerSave):                        
                        
                            foreach($jobs->entry as $entryInfo):
                                $entryNumber++;
                                $entryDetails = $EntryTable->newEntity();
                                $entryDetails->wager_id  = $wagerSave->id;
                                $entryDetails->details   = $entryInfo;
                                $entrySave = $EntryTable->save($entryDetails);
                                if($entrySave):
                                  
                                else:
                                    $errorCode[] = "305";
                                endif;                                
                            endforeach;   
                        
                    else:    
                        $errorCode[] = "304";
                    endif;
                    
                endforeach;
                
            else:
                $errorCode[] = "303"; 
            endif;

        endif;
        
        
      
        if(!empty($errorCode)):
            
            echo "<?xml version='1.0' encoding='utf-8'?>";
            echo "<response>";
            foreach($errorCode as $error):
                echo "<error>$error</error>";
            endforeach;
            echo "</response>";
            
        else:
            
            echo "<?xml version='1.0' encoding='utf-8'?>";
            echo "<response>";
            echo "<wager>$wagerNumber</wager>";
            echo "<entry>$entryNumber</entry>";
            echo "<status>Successfully uploaded</status>";
            echo "<job_id>$job_id</job_id>";
            echo "</response>";
            
        endif;
        
        exit;
        
    }
    
    
        
}
