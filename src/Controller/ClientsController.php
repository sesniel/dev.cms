<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;


use Cake\Controller\Controller;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use App\Model\Table\App\Model\Table;

use App\Controller\AppController;

/**
 * Jobs Controller
 *
 * @property \App\Model\Table\JobsTable $Jobs
 */
class ClientsController extends AppController
{
    
    public $components = ['Paginator'];
    
    public function initialize(){
        parent::initialize();        
        $this->loadComponent('Paginator');
        $this->viewBuilder()->layout("admin_v1") ;
        $user_id = ($this->request->session()->check("user_token")?$this->request->session()->read("user_token"):0);
        if (!$user_id){
                $this->redirect(array(
                                "controller" => "users",
                                "action" => "login"
                ));
        }else{        
            return $user_id;
        }
    }    

    public function index(){
        
        $UserTable = TableRegistry::get('User');
        $UserDetails = $UserTable->find('all');
        $this->set('UserDetails', $UserDetails);
        
    }
    
    
    public function details($client_id = null){
     
        if($this->request->data):
            
            $JobTable = TableRegistry::get('Jobs');
            $getJobs  = $JobTable->find('all',[
                'conditions' => [
                    'Jobs.created >=' => $this->request->data['start_date'] . " 00:00:00",
                    'Jobs.created <=' => $this->request->data['end_date'] . " 23:59:50",
                    'Jobs.user_id' => $client_id]
                ])
                    ->select(['id', 'status', 'job_id', 'created'])
                ->contain(['Wagers']);
            
//            $jobDetails = $getJobs;
            $jobDetails = $this->addEntry($getJobs->toArray());
            
            $dateInfo = $this->request->data;
            $this->set(compact('dateInfo', 'jobDetails'));
        
        endif;
    
    }
    
    private function addEntry($jobArray = null){
        $EntriesTable = TableRegistry::get('Entries');
        foreach($jobArray as $x => $job):
        $entryCounts = 0;
            foreach($job['wagers'] as $y => $wager):
                $getEntries = $EntriesTable->find('all',[
                    'conditions' => [
                        'Entries.wager_id' => $wager['id']
                    ]
                ]); 
                $entryCounts += count($getEntries->toArray());
                $jobArray[$x]['wagers'][$y]['Product_details'] = $this->addProduct($wager['product_id']);
                $jobArray[$x]['wagers'][$y]['Entries_counts'] = count($getEntries->toArray());
                $jobArray[$x]['wagers'][$y]['Entries'] = $getEntries->toArray();                
            endforeach;
            $jobArray[$x]['Entry_counts'] = $entryCounts;    
        endforeach;
        
        return $jobArray;
        
    }
    
    private function addProduct($product_id = null){
        
        $ProductsTable = TableRegistry::get('Products');
        $getProducts = $ProductsTable->find('all', [
            'conditions' => ['Products.id' => $product_id]
        ])->select('name', 'day')->first();
        return $getProducts;
    }

}
