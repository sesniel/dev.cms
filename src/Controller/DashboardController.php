<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;


use Cake\Controller\Controller;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use App\Model\Table\App\Model\Table;   

use App\Controller\AppController; 

class DashboardController extends AppController
{
    

    public $components = ['Paginator'];

    public $paginate = [
        'limit' => 5
    ];
    
    public function initialize(){
        parent::initialize();        
        $this->viewBuilder()->layout("admin_v1") ;
        $user_id = ($this->request->session()->check("user_token")?$this->request->session()->read("user_token"):0);
        if (!$user_id){
                $this->redirect(array(
                                "controller" => "users",
                                "action" => "login"
                ));
        }else{        
            return $user_id;
        }
    }
    
    public function index(){
        $this->loadModel('Jobs');
        $this->loadModel('Wagers');
        $this->loadModel('Products');
        $this->loadModel('Verify');
        $jobs = $this->Jobs->find('all',[
            'conditions' => [
                'Jobs.status' => 'Received',                
                'Jobs.status !=' => 'Delete'
            ]
        ])->contain(['Users', 'Wagers']);
        $wagers = $this->Wagers->find('all',[
            'conditions' => [
                'Wagers.status' => 'Received',                
                'Wagers.status !=' => 'Delete'
            ]
        ])
        ->contain(['Products', 'Users', 'Entries'])
        ->order(['Wagers.draw_date' => 'ASC']);
        $verifies = $this->Verify->find('all',[
            'conditions' => [
                'Verify.status' => 'Processing'
            ]
        ])->contain(['Wagers']);
        
        $products = $this->Products->find('all');
        $this->set(compact('jobs','wagers', 'products', 'verifies'));
    }
    
    public function run(){
        
        $UserTable    = TableRegistry::get('User');  
        $JobTable     = TableRegistry::get('Job');
        $WagerTable   = TableRegistry::get('Wager');
        $EntryTable   = TableRegistry::get('Entry');
        $jobInfo = array();
        $xml = simplexml_load_string($this->request->data['xml_data'], "SimpleXMLElement", LIBXML_NOCDATA);

        $token    = $xml->attributes()->token;
        $username = $xml->attributes()->username;
        $password = $xml->attributes()->password;
        
        echo crypt($password,99) . "<br/>";

        $userDetails = $UserTable->find('all')
                        ->where([
                            'User.password'    => crypt($password,99),
                            'User.username' => $username,
                            'User.token'    => $token                           
                        ])
                        ->first(); 
        
        if(!empty($userDetails)):

            
            $jobDetails = $JobTable->newEntity();
            $jobDetails->user_id      = $userDetails->id;
            $jobDetails->file_content = $this->request->data['xml_data'];
            $jobDetails->status       = "Pending";
            $saveJob = $JobTable->save($jobDetails);
            if($saveJob): 
                
                foreach($xml->jobs as $jobs): 
                    $product_id   = $jobs->attributes()->product_type;
                    $draw_date    = $jobs->attributes()->draw_date;
                    $draw_number  = $jobs->attributes()->draw_number;
                
                    $wagerDetails = $WagerTable->newEntity();
                    $wagerDetails->job_id       = $saveJob->id;
                    $wagerDetails->product_id   = $product_id;
                    $wagerDetails->draw_date    = $draw_date;
                    $wagerDetails->draw_number  = $draw_number;
                    $wagerDetails->status       = "Pending";
                    $wagerSave = $WagerTable->save($wagerDetails);
                    if($wagerSave):
                        
                        foreach($jobs->wager as $wagerInfo): 
                        
                            $wagerDetailId  = $wagerInfo->attributes()->id;
                            foreach($wagerInfo->entry as $entryInfo):
                                
                                $entryDetails = $EntryTable->newEntity();
                                $entryDetails->wager_id  = $wagerSave->id;
                                $entryDetails->detail_id = $wagerDetailId;
                                $entryDetails->details   = $entryInfo;
                                $entrySave = $EntryTable->save($entryDetails);
                                if($entrySave):
                                  
                                else:
                                    echo "Error Saving Entries";
                                endif;
                                
                            endforeach;
                    
                        endforeach;
                        
                        
                    else:    
                        echo "Error Saving wagers";
                    endif;
                    
                endforeach;
                
            else:
                echo "Error saving job information"; 
            endif;

        endif;
        
        
        exit;
        
        
        $userInfo = array(
            'email' => "next.next@yahoo.com",
            'password' => "joshua.joshua",
            'salt' => sha1("joshua"),
            'save' => $this->request->data['xml_data'],
            'password_last_update' => date("Y-m-d H:i:s")
        );
        $usersDetails = $Users->newEntity($userInfo);
        $Users->save($usersDetails);
        
        $xmlNode = simplexml_load_string($this->request->data['xml_data']);
        debug($xmlNode);
        exit;
        
        
        
    }
    
    
        
}
