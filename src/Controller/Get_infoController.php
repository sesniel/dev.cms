<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;


use Cake\Controller\Controller;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use App\Model\Table\App\Model\Table;

class Get_infoController extends AppController
{
    
    public function initialize(){
        parent::initialize();
        $this->viewBuilder()->layout("xml_display") ;
    }
    
    public function job($token = null, $jobId = null){
                
        $confirmUser['token']       = $token;
        $confirmUser['job_id']      = $jobId;
        $confirmUser['username']    = $this->request->data['username'];
        $confirmUser['password']    = $this->request->data['password'];
        
        $accessDetails = $this->confirmUser($confirmUser);
        $this->set(compact('accessDetails'));        
        
    }
    
    public function wager($token = null, $jobId = null, $wager_id = null){
                
        $confirmUser['token']       = $token;
        $confirmUser['job_id']      = $jobId;
        $confirmUser['username']    = $this->request->data['username'];
        $confirmUser['password']    = $this->request->data['password'];
        
        $accessDetails = $this->confirmUser($confirmUser);
        $passDetails = $this->getWagerInfo($accessDetails,$wager_id);
        $this->set(compact('passDetails', 'wager_id'));        
        
    }
    
    public function draw($token = null, $date = null){
                
        $confirmUser['token']       = $token;
        $confirmUser['username']    = $this->request->data['username'];
        $confirmUser['password']    = $this->request->data['password'];
        $drawInfo = "";
        $accessDetails = $this->confirmUser($confirmUser, "useronly"); //debug($accessDetails);exit;
        if($accessDetails == '301' || $accessDetails == '303' || $accessDetails == '304'):
            $drawInfo = $accessDetails;
            $this->set(compact('drawInfo'));  
        elseif($accessDetails):
            $drawInfo = $this->getDrawInfo(date("Y-m-d", strtotime($date)));
            $this->set(compact('drawInfo'));      
        endif;  
        
    }
    
    private function getDrawInfo($date = null){
        
        $WagersTable = TableRegistry::get('Wagers');
        $entryInfo = $WagersTable->find('all',[
            'conditions' => [
                'Wagers.draw_date' => $date
            ]
        ])
        ->contain('Entries')        
        ->toArray();
        return $entryInfo;
    }
    
    private function getWagerInfo($jobInfo = null, $wager_id){
       
        if($jobInfo == '301' || $jobInfo == '303'):
            return $jobInfo;
        else: 
            foreach($jobInfo['wagers'] as $wager):
                if($wager_id == $wager->detail_full):
                    $EntriesTable = TableRegistry::get('Entries');
                    $entryInfo = $EntriesTable->find('all',[
                        'conditions' => [
                            'Entries.wager_id' => $wager->id
                        ]
                    ])->toArray();
                    $submit['job_id'] = $jobInfo->job_id;
                    $submit['wager'] = $wager;
                    $submit['entries'] = $entryInfo;
                endif;
            endforeach;
            if(!empty($submit)):
                return $submit;
            else:
                return "314";
            endif;
        endif;
        
    }
    
    private function confirmUser($details = null, $type = "withjob"){
        $UserTable = TableRegistry::get('User');
        $JobTable = TableRegistry::get('Jobs');
        
        $userInfo = $UserTable->find('all',[
            "conditions" => [
                'User.username' => $details['username'],
                'User.password' => crypt($details['password'],99)
                ]
        ])
        ->select(['id', 'type', 'email', 'first_name', 'last_name', 'token', 'status'])        
        ->first();
        
        if(empty($userInfo)):
            return "304";
        elseif($details['token'] == $userInfo->token):
            return "301";
        elseif(!empty($userInfo) && $type == "withjob"):
            $jobInfo = $JobTable->find('all',[
                "conditions" => [
                    'Jobs.user_id' => $userInfo->id,
                    'Jobs.job_id' => $details['job_id']
                    ]
            ])
            ->contain(['Wagers'])
            ->first();            
            if(!empty($jobInfo)):
                return $jobInfo;
            else:
                return "303";
            endif;
        elseif(!empty($userInfo) && $type != "withjob"):
            return $userInfo;
        endif;        
        
    }
    
}
