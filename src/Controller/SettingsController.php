<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;


use Cake\Controller\Controller;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use App\Model\Table\App\Model\Table;

class SettingsController extends AppController
{
    
    public function initialize(){
        parent::initialize();        
        $this->viewBuilder()->layout("admin_v1") ;
        $user_id = ($this->request->session()->check("user_id")?$this->request->session()->read("user_id"):0);
        if (!$user_id){
                $this->redirect(array(
                                "controller" => "users",
                                "action" => "login"
                ));
        }else{
        
            $UserTable = TableRegistry::get('User');
            $userInfoData = $UserTable->find("all",[
                                    "conditions" => [
                                                    "User.id" => $user_id
                                    ]
                    ])
                    ->select(['User.id','User.first_name', 'User.last_name', 'User.middle_name', 'User.email', 'User.password_last_update', 'User.image', 'User.password'])      
                    ->first();
            $this->set('userInfoData', $userInfoData);
            return $userInfoData;
        }
    }
    
    public function index(){
        
        $userInfo = $this->initialize();
        $this->set(compact('userInfo'));
        
    }
    
    public function name(){
        
        $UserDetails = $this->initialize(); 
//        pr($UserDetails);exit;
        $this->set(compact('UserDetails'));
        $this->render('update_account');
    }
    
    public function profileimage(){
                   
        $user_id = $this->initialize();          
        $UserTable = TableRegistry::get('User');  
//        $image = $UserTable->newEntity();
        
        if ($this->request->is('post')) {
                $file = $this->request->data('Images');
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                
                $usersDetails = $UserTable->get($user_id['id']);
                $usersDetails->image = $user_id['id'].'.'. $ext;
                if($UserTable->save($usersDetails) && move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/profile_image/' .$user_id['id'].'.'. $ext)):
                    $this->redirect(array(
                        "action" => "index"
                    ));
                else:                
                    $this->Flash->set('Something wrong uploading, please try again.', 'default', [
                        'class' => 'alert alert-danger top-general-alert'
                    ]);
                endif;
        }
        
        $this->render('update_account');
    }
     
    public function contact(){
        $UserDetails = $this->initialize();
        
        $this->set(compact('UserDetails'));
        $this->render('update_account');
    }
    
    public function savesettings(){
        
        if($this->request->is('post')):
            $user_id = $this->initialize();
            $UserTable = TableRegistry::get('User');
            $usersDetails = $UserTable->get($user_id['id']);
            $usersDetails->phone = $this->request->data("phone");
            $usersDetails->first_name = $this->request->data("first_name");
            $usersDetails->last_name = $this->request->data("last_name");
            $usersDetails->middle_name = $this->request->data("middle_name");
            if($UserTable->save($usersDetails)):
                if($this->request->data("last_name") != "" && $this->request->data("first_name") != ""):
                    $this->request->session()->write('first_name', $this->request->data("first_name"));
                    $this->request->session()->write('last_name', $this->request->data("last_name"));
                endif;
                $this->redirect(array(
                    "action" => "index"
                ));
            endif;
        endif;
    }
      
    public function password(){
        $UserDetails = $this->initialize();  
        $this->set(compact('UserDetails'));
        
        if($this->request->is('post')):
            
            $current = crypt($this->request->data['current'],99);
            $password = crypt($this->request->data['password'],99);
            $confirm = crypt($this->request->data['confirm'],99);
            
            if($UserDetails['password'] == $current && $password == $confirm):
                
            
                $UserTable = TableRegistry::get('User');
                $usersDetails = $UserTable->get($UserDetails['id']);
                $usersDetails->password = $password;


                if($UserTable->save($usersDetails)):
                    $this->redirect(array(
                        "controller" => 'users',
                        "action" => "index"
                    ));
                endif;
            else:         
                    $this->Flash->set('Sorry something wrong in changing your password, please try again.', [
                                    'class' => 'warning'
                    ]);
                    $this->redirect($this->referer());
            endif;
            
            
            
        endif;
        $this->render('update_account');
    } 
    
}
