<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;


use Cake\I18n\Time;
use Cake\Controller\Controller;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use App\Model\Table\App\Model\Table;

use App\Controller\AppController;

/**
 * Wagers Controller
 *
 * @property \App\Model\Table\WagersTable $Wagers
 */
class WagersController extends AppController
{
    
    public function initialize(){
        parent::initialize();        
        $this->viewBuilder()->layout("admin_v1") ;
        $user_id = ($this->request->session()->check("user_token")?$this->request->session()->read("user_token"):0);
        if (!$user_id){
                $this->redirect(array(
                                "controller" => "users",
                                "action" => "login"
                ));
        }else{        
            return $user_id;
        }
    }
    
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Jobs', 'Products']
        ];
        $wagers = $this->paginate($this->Wagers);

        $this->set(compact('wagers'));
        $this->set('_serialize', ['wagers']);
    }
    
    public function viewwagerdetailes($date=null){
        $this->viewBuilder()->layout("ajax_view");
        
        $this->loadModel('Wagers');
        $this->loadModel('Products');
        $wagers = $this->Wagers->find('all',[
            'conditions' => [
                'Wagers.status' => 'Received',                
                'Wagers.status !=' => 'Delete',             
                'Wagers.draw_date' => $date
            ]
        ])
        ->contain(['Users', 'Entries'])
        ->order(['Wagers.draw_date' => 'ASC']);
        $products = $this->Products->find('all');
        $this->set(compact('wagers', 'products', 'date'));
        
    }
    
    public function viewclientdetailes($id=null){
        
        $this->viewBuilder()->layout("ajax_view");        
        $this->loadModel('Wagers');
        $this->loadModel('User');
        $this->loadModel('Products');
        $wagers = $this->Wagers->find('all',[
            'conditions' => [
                'Wagers.status' => 'Received',                
                'Wagers.status !=' => 'Delete',             
                'Wagers.user_id' => $id
            ]
        ])
        ->contain(['Entries', 'Users'])
        ->order(['Wagers.draw_date' => 'ASC']);
        $user = $this->User->find('all', [
                    'conditions' => [
                        'User.id' => $id
                    ]
                ])
                ->select(['type', 'email', 'first_name', 'last_name'])
                ->first(); 
        $products = $this->Products->find('all');
        $this->set(compact('wagers', 'products', 'user'));
        
    }
    
    public function exportdate($date){        
        
        $this->loadModel('Wagers');
        $wagers = $this->Wagers->find('all',[
            'conditions' => [
                'Wagers.status' => 'Received',                
                'Wagers.status !=' => 'Delete',             
                'Wagers.draw_date' => $date
            ]
        ])
        ->contain(['Entries'])
        ->order(['Wagers.draw_date' => 'ASC']);   
        $this->exportwager($wagers);
        
    }
    
    public function exportclient($id = null){
          
        $this->loadModel('Wagers');
        $wagers = $this->Wagers->find('all',[
            'conditions' => [
                'Wagers.status' => 'Received',                
                'Wagers.status !=' => 'Delete',             
                'Wagers.user_id' => $id
            ]
        ])
        ->contain(['Entries'])
        ->order(['Wagers.draw_date' => 'ASC']);
        $this->exportwager($wagers);
        
    }
    
    public function exportwager($wagerDetails = null){
        

        $VerifyTable     = TableRegistry::get('Verify');
        $WagersTable     = TableRegistry::get('Wagers');
        $ver_id = "Verify_" . sha1(rand());
        $verDetails = $VerifyTable->newEntity();
        $verDetails->status       = "Processing";
        $verDetails->verify_id       = $ver_id;
//        
        $saveVerify = $VerifyTable->save($verDetails);
        $idVerify = $saveVerify->id;
        
        if($saveVerify):
            

            header('Content-type: text/xml');
            header('Content-Disposition: attachment; filename="text.xml"');
            $xml_contents = '<?xml version="1.0" encoding="UTF-8"?>';
            $xml_contents .= '<header>';
            foreach($wagerDetails as $wager):
                $wagerSave = $WagersTable->get($wager->id);
                $wagerSave->status = "Processing";
                $wagerSave->verify_id = $idVerify;
                $wagerSave->process_date = Time::now();
                $WagersTable->save($wagerSave);

                $prodId = $this->verifyproductid($wager->draw_date);
                $xml_contents .= '<wager id="'.$wager->detail_full.'"  draw_number="'.$wager->draw_number.'"  draw_date="'.$wager->draw_date.'" product_type="'.$prodId.'"> ';
                    foreach($wager->entries as $entry):
                        $xml_contents .= '<entry>'.$entry->details.'</entry>';
                    endforeach;
                $xml_contents .= '</wager>';

            endforeach;        
            $xml_contents .= '</header>';
            echo $xml_contents;
        
        endif;
        exit;
    }
    
    private function verifyproductid($date = null){
        
        $this->loadModel('Products');
        $products = $this->Products->find('all');
        $day = date('l',strtotime($date) );  
        $prodDisplay = "Error";
        foreach($products as $prod):
            if($prod->day == $day):
                $prodDisplay = $prod->name;
            endif;
        endforeach;
        return $prodDisplay;
                
    }

    /**
     * View method
     *
     * @param string|null $id Wager id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $wager = $this->Wagers->get($id, [
            'contain' => ['Jobs', 'Products', 'Entries']
        ]);

        $this->set('wager', $wager);
        $this->set('_serialize', ['wager']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $wager = $this->Wagers->newEntity();
        if ($this->request->is('post')) {
            $wager = $this->Wagers->patchEntity($wager, $this->request->data);
            if ($this->Wagers->save($wager)) {
                $this->Flash->success(__('The wager has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The wager could not be saved. Please, try again.'));
            }
        }
        $jobs = $this->Wagers->Jobs->find('list', ['limit' => 200]);
        $products = $this->Wagers->Products->find('list', ['limit' => 200]);
        $this->set(compact('wager', 'jobs', 'products'));
        $this->set('_serialize', ['wager']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Wager id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $wager = $this->Wagers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $wager = $this->Wagers->patchEntity($wager, $this->request->data);
            if ($this->Wagers->save($wager)) {
                $this->Flash->success(__('The wager has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The wager could not be saved. Please, try again.'));
            }
        }
        $jobs = $this->Wagers->Jobs->find('list', ['limit' => 200]);
        $products = $this->Wagers->Products->find('list', ['limit' => 200]);
        $this->set(compact('wager', 'jobs', 'products'));
        $this->set('_serialize', ['wager']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Wager id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $wager = $this->Wagers->get($id);
        if ($this->Wagers->delete($wager)) {
            $this->Flash->success(__('The wager has been deleted.'));
        } else {
            $this->Flash->error(__('The wager could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
