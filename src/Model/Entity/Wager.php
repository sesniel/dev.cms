<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Wager Entity
 *
 * @property int $id
 * @property int $job_id
 * @property string $product_id
 * @property string $draw_number
 * @property string $draw_date
 * @property string $ticket
 * @property \Cake\I18n\Time $process_date
 * @property string $fsn
 * @property string $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Job $job
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Entry[] $entries
 */
class Wager extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
