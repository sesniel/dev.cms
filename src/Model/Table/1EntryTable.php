<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Phinx\Db\Table\ForeignKey;

class EntryTable extends Table {
    public function initialize(array $config) {
        $this->table('entries');
        $this->belongsTo(
                'Wager', [
                    'foreignKey' => "wager_id",
                    'className' => 'Wager'
                ]
        );
    }
}
