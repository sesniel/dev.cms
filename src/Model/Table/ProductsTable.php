<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Phinx\Db\Table\ForeignKey;

class ProductsTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('products');
	}
}