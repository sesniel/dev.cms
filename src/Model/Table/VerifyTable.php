<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class VerifyTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('verify');
        $this->addBehavior('Timestamp');
        
        
        $this->hasMany('Wagers', [
            'foreignKey' => 'verify_id'
        ]);
    }
}