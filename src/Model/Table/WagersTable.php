<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Wagers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Jobs
 * @property \Cake\ORM\Association\BelongsTo $Products
 * @property \Cake\ORM\Association\HasMany $Entries
 *
 * @method \App\Model\Entity\Wager get($primaryKey, $options = [])
 * @method \App\Model\Entity\Wager newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Wager[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Wager|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Wager patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Wager[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Wager findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WagersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('wagers');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Jobs', [
            'foreignKey' => 'job_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Verify', [
            'foreignKey' => 'verify_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Entries', [
            'foreignKey' => 'wager_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
//    public function validationDefault(Validator $validator)
//    {
//        $validator
//            ->integer('id')
//            ->allowEmpty('id', 'create');
//
//        $validator
//            ->requirePresence('draw_number', 'create')
//            ->notEmpty('draw_number');
//
//        $validator
//            ->requirePresence('draw_date', 'create')
//            ->notEmpty('draw_date');
//
//        $validator
//            ->requirePresence('ticket', 'create')
//            ->notEmpty('ticket');
//
//        $validator
//            ->date('process_date')
//            ->requirePresence('process_date', 'create')
//            ->notEmpty('process_date');
//
//        $validator
//            ->requirePresence('fsn', 'create')
//            ->notEmpty('fsn');
//
//        $validator
//            ->requirePresence('status', 'create')
//            ->notEmpty('status');
//
//        return $validator;
//    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
//    public function buildRules(RulesChecker $rules)
//    {
//        $rules->add($rules->existsIn(['job_id'], 'Jobs'));
//        $rules->add($rules->existsIn(['product_id'], 'Products'));
//
//        return $rules;
//    }
}
