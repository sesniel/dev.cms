<style>
    
    .span-one{float: left; text-align: left; width:550px;}
    .span-two{float: left; text-align: left; width:250px; padding-left:50px;}
    .span-two-none{float: left; text-align: left; width:250px;}
    .span-three{float: left; text-align: left; width:180px;}
    
</style>



<link href="/datepicker/css/1bootstrap.min.css" rel="stylesheet" media="screen">
<link href="/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    
<div class="container">
    
<?php 
    echo $this->Form->create(null, [
    'url' => ['action' => 'details',$this->request->params['pass'][0]],
    'type' => 'post', 'class' => 'form-horizontal']); 
?>
    <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="exampleInputEmail1">Start Date</label>
                <div class="input-group date form_date_start" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1" data-link-format="yyyy-mm-dd">
                    <input class="form-control" size="16" type="text" value="<?php if(isset($dateInfo['start_date'])): echo $dateInfo['start_date']; endif; ?>" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
		<input type="hidden" id="dtp_input1" name="start_date" value="<?php if(isset($dateInfo['start_date'])): echo $dateInfo['start_date']; endif; ?>" />
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="exampleInputEmail1">End Date</label>
                <div class="input-group date form_date_end" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <input class="form-control" size="16" type="text" value='<?php if(isset($dateInfo['end_date'])): echo $dateInfo['end_date']; endif; ?>' readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
		<input type="hidden" id="dtp_input2" name="end_date" value="<?php if(isset($dateInfo['end_date'])): echo $dateInfo['end_date']; endif; ?>" />
            </div>
        </div>
        
    </div>
    <div class="row" style="padding-bottom: 15px;">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <input type="submit" class="btn btn-success pull-right" value="Submit">
        </div>
    </div>
<?php echo $this->Form->end(); ?>
    
</div>
    
    <hr style="padding-bottom: 15px;"/>

<?php if(isset($jobDetails)): ?>    
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            
            <?php foreach($jobDetails as $key => $details): 
    $checkNum = 0;?>
            
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="heading<?= $key ?>">
                    <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $key ?>" aria-expanded="true" aria-controls="collapse<?= $key ?>">
                        
                          <span class="span-one">Job ID: <?= $details['job_id']   ?> <br/></span><br/>
                          <span class="span-two-none">Wagers Count: <?php echo count($details['wagers'])   ?>  </span>
                          <span class="span-two-none">Entries Count: <?= $details['Entry_counts']   ?><br/></span><br/>
                          <span class="span-two-none">Status: <?= $details['status']   ?> </span>
                          <span class="span-two-none">Date Uploaded: <?php echo date("d-m-Y", strtotime($details['created']));   ?><br/></span><br/>
                      
                      </a>
                    </h4>
                  </div>
                  <div id="collapse<?= $key ?>" class="panel-collapse collapse <?php echo ( $key == 1101122 ? "in" : "out"); ?> " role="tabpanel" aria-labelledby="heading<?= $key ?>">
                    <div class="panel-body">
                        <div class='row'>
                            <?Php foreach($details['wagers'] as $wagerDetail): ?>
                                <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                                    <span class="span-three">Draw Date:<label> <?= $wagerDetail['draw_date']   ?></label></span>
                                    <span class="span-two">Draw Number:<label> <?= $wagerDetail['draw_number']   ?></label><br/></span><br/>
                                    
                                    <span class="span-three">Ticket:<label> <?= $wagerDetail['ticket']   ?></label></span>
                                    <span class="span-two">process_date:<label> <?= $wagerDetail['process_date']   ?></label><br/></span><br/>
                                    
                                    <span class="span-three"><label><?= $wagerDetail['Product_details']['name']   ?></label></span>
                                    <span class="span-two">FSN:<label> <?= $wagerDetail['fsn']   ?></label><br/></span><br/>
                                    
                                    <hr style='width:80%;'/>
                                    <div class='row'>
                                        <table class="table" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <th><?= __('Detail Id') ?></th>
                                                <th><?= __('Lotto Number') ?></th>
                                            </tr>
                                        <?php 
                                            foreach($wagerDetail['Entries'] as $entry): 
                                                
                                        ?>
                                            <tr>
                                                <td><?= h($entry['detail_full' . '']) ?></td>
                                                <td>
                                                    <?php 
                                                        $ticketNumber = str_split($entry['details'],2);
                                                        $num = count($ticketNumber);
                                                        $checkNumberDetails = count($wagerDetail['Entries']);
                                                        
                                                        if($checkNum < $checkNumberDetails):
                                                            $checkNum = count($wagerDetail['Entries']);
                                                        endif;
                                                        $x = 0;
                                                        foreach($ticketNumber as $ticketD):
                                                            echo $ticketD;
                                                            $x++;
                                                            if($x < $num):
                                                               echo " | "; 
                                                            endif;
                                                        endforeach;
                                                        
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php endforeach;  ?>
                                            
                                        </table>
                                        <?php 
                                        
                                        
                                        
                                                        if($checkNum > $checkNumberDetails):
//                                                            echo " --> $checkNum >= $checkNumberDetails";
                                                            for($check = $checkNumberDetails;$check < $checkNum;$check++):
                                                                echo "<br/>";
                                                            endfor;
                                                            echo "<br/>";
                                                            echo "<br/>";
                                                            echo "<br/>";
                                                        endif;
                                        ?>
                                    </div>
                                    
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                  </div>
                </div>
            
            <?php endforeach; ?>
            </div>
        </div>
    </div> 
    <?php // debug($jobDetails); ?>
<?php endif; ?>   
    
    

<script type="text/javascript" src="/js/jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="/datepicker/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/datepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="/datepicker/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script type="text/javascript">
	$('.form_date_start').datetimepicker({
            language:  'en',
            weekStart: 1,
            todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0
        });
        
	$('.form_date_end').datetimepicker({
            language:  'en',
            weekStart: 1,
            todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0
        });
        
        $('.form_date_start').val('2016-08-21');
        $('.form_date_end').val('2016-08-21');
</script>


