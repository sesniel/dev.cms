
<div class="row" style="padding-top: 20px; margin: auto;">
<?php foreach($UserDetails as $detail): ?>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding: 15px;">            
            <?= $this->Html->link(
                        __('<i class="fa fa-user fa-5x"></i><br/>' . " <label style='width: 100px;'>" . $detail['first_name'] . " " . $detail['last_name'] . "</label><br>"), 
                        ['controller' => 'clients', 'action' => 'details',$detail['id'] ],['escape' => false, 'class' => 'btn btn-sq-lg btn-primary', 'style' => 'padding: 35px;']) ?>
        </div>
	
<?php endforeach; ?>
</div>