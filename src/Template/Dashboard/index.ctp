<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<h1>Dashboard</h1>

    <div class="dialogBox" style="border:1px solid gray;">
    </div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-dark-header">
            <tr class="success">
                <th colspan="5" class="text-center">Processing</th>
            </tr>
            <tr>
                <th>Job ID</th>
                <th>Status</th>
                <th>Wagers</th>
                <th>Action</th>                
            </tr>
            
            <?php foreach($verifies as $verify): ?>
                <tr>
                    <td><?= $verify->verify_id ?></td>
                    <td><?= $verify->status ?></td>
                    <td><?php echo count($verify->wagers) ?></td>
                    <td>                    
                        <?= $this->Html->link('<span class="text">Export</span>', 
                                ['controller' => 'jobs', 'action' => 'index'],['escape' => false, 'class' => 'btn btn-xs btn-primary']) 
                        ?>              
                        <?= $this->Html->link('<span class="text">Report</span>', 
                                ['controller' => 'jobs', 'action' => 'index'],['escape' => false, 'class' => 'btn btn-xs btn-primary']) 
                        ?>              
                        <?= $this->Html->link('<span class="text">Delete</span>', 
                                ['controller' => 'jobs', 'action' => 'index'],['escape' => false, 'class' => 'btn btn-xs btn-danger']) 
                        ?>
                    </td>                
                </tr>
            <?php endforeach; ?>
                
        </table>
    </div>
</div>
<?php
    $date_wager = array();
    $client_wager = array();
    $tmpDate = "";
    foreach($wagers as $wagerDisplay):
        
        $date_wager[$wagerDisplay->draw_date][] = $wagerDisplay;
        $client_wager[$wagerDisplay->user->id][] = $wagerDisplay;
        
    endforeach;
?>
<hr/><br/><br/>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-dark-header">
            <tr class="success">
                <th colspan="5" class="text-center">Submitted Wagers</th>
            </tr>
            <tr>
                <th>Draw Date</th>
                <th>Product</th>
                <th>Wagers</th>   
                <th>Action</th>                 
            </tr>
            <?php $x = 0; foreach($date_wager as $key => $wagerDetail): $x++; //debug($wagerDetail); ?>
                
                <?php 
                    $lottoDay = date('l', strtotime($key)); 
                    $prodDisplay = "Invalid Product Date";
                    foreach($products as $prod):
                        if($lottoDay == $prod->day):
                            $prodDisplay = $prod->name;
                        endif;
                    endforeach;
                ?>
            
                <tr <?php if($prodDisplay == "Invalid Product Date"): echo 'class="danger"'; endif; ?>>
                    <td>
                        <?= $key ?>
                    </td>
                    <td>
                        <?php
                            echo $prodDisplay;
                        ?>
                    </td>
                    <td><?php echo count($date_wager[$key]); ?></td>   
                    <td>                 
                        <?= $this->Html->link('<span class="text">View Details</span>', 
                                ['controller' => 'wagers', 'action' => 'viewwagerdetailes',$key],['escape' => false, 'class' => "btn btn-xs btn-primary exampleLink$x"]) 
                        ?>                 
                        <?= $this->Html->link('<span class="text">Export</span>', 
                                ['controller' => 'wagers', 'action' => 'exportdate',$key],['escape' => false, 'class' => 'btn btn-xs btn-primary', 'target' => '_blank', "id" => "exportLink$x" ]) 
                        ?>   
                    </td>              
                </tr>
                
                <script type="text/javascript">

                    var $modalDialog = $('<div/>', { 
                      'class': 'exampleModal<?php echo $x; ?>', 
                      'id': 'exampleModal1<?php echo $x; ?>'
                    })
                    .appendTo('body')
                    .dialog({
                        resizable: false,
                        autoOpen: false,
                        height: $( window ).height(),
                        width: $( window ).width(),
                        draggable: true,
                        show: 'fold',
                        buttons: {
                            "Exit": function () {
                                $modalDialog.dialog("close");
                            }
                        },
                        modal: true
                    });
                    
                    $('#exportLink<?php echo $x; ?>').click(function() {
                        window.setTimeout('location.reload()', 3000);
                    });

                    $(function () {
                        $('a.exampleLink<?php echo $x; ?>').on('click', function (e) {
                            e.preventDefault();
                            // TODO: Undo comments, below
                            var url = $('a.exampleLink<?php echo $x; ?>:first').attr('href');
                            $modalDialog.load(url);
                            $modalDialog.dialog("open");
                        });
                    });

                </script>
                
            <?php endforeach; ?>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-dark-header">
            <tr class="success">
                <th colspan="5" class="text-center">Cleints</th>
            </tr>
            <tr>
                <th>Client Name</th>
                <th>Wagers</th>
                <th>Entries</th>   
                <th>Action</th>                 
            </tr>
            <?php 
                $y = 0; foreach($client_wager as $key => $wagerClient): $y++;
                    $entriesCount = 0;
                    foreach($wagerClient as $wagerClientDetail):
                        $entriesCount += count($wagerClientDetail->entries);
                    endforeach;
            ?>
                <tr>
                    <td><?= $wagerClient[0]->user->first_name ?> <?= $wagerClient[0]->user->last_name ?></td>
                    <td><?php echo count($wagerClient); ?></td>
                    <td><?= $entriesCount ?></td>   
                    <td>                
                        <?= $this->Html->link('<span class="text">View Details</span>', 
                                ['controller' => 'wagers', 'action' => 'viewclientdetailes',$wagerClient[0]->user->id],['escape' => false, 'class' => "btn btn-xs btn-primary clientLink$y"]) 
                        ?>   
                        
                        <?= $this->Html->link('<span class="text">Export</span>', 
                                ['controller' => 'wagers', 'action' => 'exportclient',$wagerClient[0]->user->id],['escape' => false, 'class' => 'btn btn-xs btn-primary', 'target' => '_blank', "id" => "exportClient$y" ]) 
                        ?> 
                        
                    </td>              
                </tr>
                               
                <script type="text/javascript">
                    
                    var $modalDialog = $('<div/>', { 
                      'class': 'clientModal<?php echo $y; ?>', 
                      'id': 'clientModal1<?php echo $y; ?>'
                    })
                    .appendTo('body')
                    .dialog({
                        resizable: false,
                        autoOpen: false,
                        height: $( window ).height(),
                        width: $( window ).width(),
                        draggable: true,
                        show: 'fold',
                        buttons: {
                            "Exit": function () {
                                $modalDialog.dialog("close");
                            }
                        },
                        modal: true
                    });

                    $(function () {

                        $('#exportClient<?php echo $y; ?>').click(function() {
                            window.setTimeout('location.reload()', 3000);
                        });

                        $('a.clientLink<?php echo $y; ?>').on('click', function (e) {
                            e.preventDefault();
                            // TODO: Undo comments, below
                            var url = $('a.clientLink<?php echo $y; ?>:first').attr('href');
                            $modalDialog.load(url);
                            $modalDialog.dialog("open");
                        });
                    });

                </script>
                                
            <?php endforeach; ?>
        </table>
    </div>
</div>
        
 
    