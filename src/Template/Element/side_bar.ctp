

<!-- main-nav -->
<nav class="main-nav">
    <div>
        <br/><br/>
    </div>
        <ul class="main-menu">
            <li>
                <?= $this->Html->link(
                        __('<i class="fa fa-dashboard fa-fw"></i><span class="text">Dashboard</span>'), 
                        ['controller' => 'dashboard', 'action' => 'index'],['escape' => false]) ?>
            </li>
            <li>
                <?= $this->Html->link(
                        __('<i class="fa fa-list-alt fa-fw"></i><span class="text">Jobs</span>'), 
                        ['controller' => 'jobs', 'action' => 'index'],['escape' => false]) 
                ?>
            </li>
<!--            <li>
                <?= $this->Html->link(
                        __('<i class="fa fa-list-alt fa-fw"></i><span class="text">Wagers</span>'), 
                        ['controller' => 'wagers', 'action' => 'index'],['escape' => false]) ?>
            </li>-->
            <li>
                <?= $this->Html->link(
                        __('<i class="fa fa-user fa-fw"></i><span class="text">Client Management</span>'), 
                        ['controller' => 'clients', 'action' => 'index'],['escape' => false]) ?>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-file fa-fw"></i><span class="text">Verification Files</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-list-alt fa-fw"></i><span class="text">Admin Users</span>
                </a>
            </li>
        </ul>
</nav>
<!-- /main-nav -->
<div class="sidebar-minified js-toggle-minified">
        <i class="fa fa-angle-left"></i>
</div>
<!-- end sidebar content -->