<!-- TOP BAR -->
<div class="top-bar">
        <div class="container">
                <div class="row">
                        <!-- logo -->
                        <div class="col-md-2">
                                <a href="/">
                                    <label style="color:#fff; padding-top: 10px;">CMS</label>
                                </a>
                        </div>
                        <!-- end logo -->
                        <div class="col-md-10">
                                <div class="row">
                                        <div class="col-md-12">
                                                <div class="top-bar-right">
                                                        <!-- logged user and the menu -->
                                                        <div class="logged-user">
                                                                <div class="btn-group">
                                                                        <a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                                                                
                                                                                <span class="name">
                                                                                    <?php
                                                                                        if($this->request->session()->read("first_name") != "" && $this->request->session()->read("last_name") != ""):
                                                                                            echo $this->request->session()->read("first_name") . " " . $this->request->session()->read("last_name");
                                                                                        else:
                                                                                            echo "New User";
                                                                                        endif;
                                                                                    ?>
                                                                                </span> <span class="caret"></span>
                                                                        </a>
                                                                        <ul class="dropdown-menu" role="menu">
                                                                                <li>
                                                                                        <a href="/settings/index">
                                                                                                <i class="fa fa-cog"></i>
                                                                                                <span class="text">Settings</span>
                                                                                        </a>
                                                                                </li>
                                                                                <li>
                                                                                        <a href="/users/logout">
                                                                                                <i class="fa fa-power-off"></i>
                                                                                                <span class="text">Logout</span>
                                                                                        </a>
                                                                                </li>
                                                                        </ul>
                                                                </div>
                                                        </div>
                                                        <!-- end logged user and the menu -->
                                                </div>
                                                <!-- /top-bar-right -->
                                        </div>
                                </div>
                                <!-- /row -->
                        </div>
                </div>
                <!-- /row -->
        </div>
        <!-- /container -->
</div>
<!-- /top -->