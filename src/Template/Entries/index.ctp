<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Entry'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wagers'), ['controller' => 'Wagers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wager'), ['controller' => 'Wagers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="entries index large-9 medium-8 columns content">
    <h3><?= __('Entries') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('wager_id') ?></th>
                <th><?= $this->Paginator->sort('detail_full') ?></th>
                <th><?= $this->Paginator->sort('details') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($entries as $entry): ?>
            <tr>
                <td><?= $this->Number->format($entry->id) ?></td>
                <td><?= $entry->has('wager') ? $this->Html->link($entry->wager->id, ['controller' => 'Wagers', 'action' => 'view', $entry->wager->id]) : '' ?></td>
                <td><?= h($entry->detail_full) ?></td>
                <td><?= h($entry->details) ?></td>
                <td><?= h($entry->created) ?></td>
                <td><?= h($entry->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $entry->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $entry->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $entry->id], ['confirm' => __('Are you sure you want to delete # {0}?', $entry->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
