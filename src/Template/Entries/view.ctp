<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Entry'), ['action' => 'edit', $entry->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Entry'), ['action' => 'delete', $entry->id], ['confirm' => __('Are you sure you want to delete # {0}?', $entry->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Entries'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Entry'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wagers'), ['controller' => 'Wagers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wager'), ['controller' => 'Wagers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="entries view large-9 medium-8 columns content">
    <h3><?= h($entry->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Wager') ?></th>
            <td><?= $entry->has('wager') ? $this->Html->link($entry->wager->id, ['controller' => 'Wagers', 'action' => 'view', $entry->wager->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Detail Full') ?></th>
            <td><?= h($entry->detail_full) ?></td>
        </tr>
        <tr>
            <th><?= __('Details') ?></th>
            <td><?= h($entry->details) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($entry->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($entry->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($entry->modified) ?></td>
        </tr>
    </table>
</div>
