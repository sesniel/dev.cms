<?php if($drawInfo == '301' || $drawInfo == '303' || $drawInfo == '304'): ?>
<response>
    <error><?= $drawInfo ?></error>
</response>
<?php else: ?><?xml version='1.0' encoding='utf-8'?>
<response>
    <?php if(!empty($drawInfo)): ?>
    <?php foreach($drawInfo as $wager): ?>
        <wager status="<?= $wager->status ?>" id="<?= $wager->detail_full ?>" draw_number="<?= $wager->draw_number ?>" draw_date="<?= $wager->draw_date ?>" product_type="<?= $wager->product_id ?>">
            <?php foreach($wager->entries as $entries): ?>
                <entry><?= $entries['details'] ?></entry>
            <?php endforeach; ?>            
        </wager>
    <?php endforeach; ?>
    <?php else: ?>
        <wager>No available Wager to display</wager>
    <?php endif; ?>
</response>
<?php endif; ?>
