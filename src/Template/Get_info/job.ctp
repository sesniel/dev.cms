<?php if($accessDetails == 303 || $accessDetails == 301): ?><response>
	<response>
		<error><?= $accessDetails ?></error>
	</response>
</response>
<?php else: ?><?xml version='1.0' encoding='utf-8'?>
<response>
        <job_id><?= $accessDetails['job_id'] ?></job_id>
        <wagers >
            <?php foreach($accessDetails['wagers'] as $wager): ?>
                <status id='<?= $wager['detail_full'] ?>'><?= $wager['status'] ?></status>
            <?php endforeach; ?>
        </wagers>
</response>
<?php endif; ?>
