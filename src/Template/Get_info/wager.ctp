<?php if($passDetails == "301" || $passDetails == "303" || $passDetails == "314"): ?><response>
    <response>
            <error><?= $passDetails ?></error>
    </response>
</response>
<?php else: ?>
<?xml version='1.0' encoding='utf-8'?>
<response>
        <job_id> <?= $passDetails['job_id'] ?> </job_id>
        <id> <?= $passDetails['wager']->detail_full ?> </id>
        <status> <?= $passDetails['wager']->status ?>  </status>
        <wager>
            <?php foreach($passDetails['entries'] as $entry): ?>
                <entry><?= $entry['details'] ?></entry>
            <?php endforeach; ?>
        </wager>
</response>
<?php endif; ?>