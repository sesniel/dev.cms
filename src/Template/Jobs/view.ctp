<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Job'), ['action' => 'edit', $job->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Job'), ['action' => 'delete', $job->id], ['confirm' => __('Are you sure you want to delete # {0}?', $job->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Jobs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Job'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Jobs'), ['controller' => 'Jobs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Job'), ['controller' => 'Jobs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wagers'), ['controller' => 'Wagers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wager'), ['controller' => 'Wagers', 'action' => 'add']) ?> </li>
    </ul>
</nav>-->
<div class="row">
    
    <div class="col-lg-6">
        <h2><?= $job->has('user') ? $this->Html->link($job->user->first_name . " " . $job->user->last_name, ['controller' => 'Users', 'action' => 'view', $job->user->id]) : '' ?></h2>
        <?= $job->job_id ?>
    </div>
    
</div>
<div class="jobs view large-9 medium-8 columns content">
    
    <div class="related">
        <h4><?= __('Related Wagers') ?></h4>
        <?php if (!empty($job->wagers)): ?>
        <table class="table" cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Status') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Draw Number') ?></th>
                <th><?= __('Draw Date') ?></th>
                <th><?= __('Ticket') ?></th>
                <th><?= __('Process Date') ?></th>
                <th><?= __('Fsn') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($job->wagers as $wagers): ?>
            <tr>
                <td><?= h($wagers->status) ?></td>
                <td>
                    <?php 

                        foreach($productDetails as $prod):
                            
                            if($prod->id == $wagers->product_id):
                                echo $prod->name;
                            endif;
                            
                        endforeach;
                        
                    ?>
                </td>
                <td><?= h($wagers->draw_number) ?></td>
                <td><?= h($wagers->draw_date) ?></td>
                <td><?= h($wagers->ticket) ?></td>
                <td><?= h($wagers->process_date) ?></td>
                <td><?= h($wagers->fsn) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Wagers', 'action' => 'view', $wagers->id]) ?>
                    <?php //echo $this->Html->link(__('Edit'), ['controller' => 'Wagers', 'action' => 'edit', $wagers->id]) ?>
                    <?php //echo $this->Form->postLink(__('Delete'), ['controller' => 'Wagers', 'action' => 'delete', $wagers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wagers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
