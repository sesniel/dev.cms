<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<title>Dashboard</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="KingAdmin - Bootstrap Admin Dashboard Theme">
	<meta name="author" content="The Develovers">
	<!-- CSS -->
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('font-awesome.min.css') ?>
        <?= $this->Html->css('main.css') ?>
        <?= $this->Html->css('my-custom-styles.css') ?>
        
	<!--[if lte IE 9]>
            <?= $this->Html->css('main-ie.css') ?>
            <?= $this->Html->css('main-ie-part2.css') ?>
	<![endif]-->
	<!-- CSS for demo style switcher. you can remove this -->
        
	<!-- Fav and touch icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/kingadmin-favicon144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/kingadmin-favicon114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/kingadmin-favicon72x72.png">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="ico/kingadmin-favicon57x57.png">
	<link rel="shortcut icon" href="ico/favicon.png">
</head>

<body class="dashboard">
	<!-- WRAPPER -->
        
<div class="wrapper">
    <?php echo $this->element('top_bar'); ?>
    <!-- BOTTOM: LEFT NAV AND RIGHT MAIN CONTENT -->
    
    <!-- END BOTTOM: LEFT NAV AND RIGHT MAIN CONTENT -->
        

<div class="bottom">
        <div class="container">
                <div class="row">
                    <!-- left sidebar -->
                    <div class="col-md-2 left-sidebar">
                        <?php echo $this->element('side_bar'); ?>
                    </div>
                    <!-- content-wrapper -->
                    <div class="col-md-10 content-wrapper">
                        <?= $this->fetch('content') ?>
                    </div>
                    <!-- /content-wrapper -->
                </div>
                <!-- /row -->
        </div>
        <!-- /container -->
</div>
</div> 
	<!-- /wrapper -->
	<!-- FOOTER -->
	<footer class="footer">
		&copy; 2016 cmslotteries.com.au
	</footer>
	<!-- END FOOTER -->
	<!-- Javascript -->
    <?= $this->Html->script('jquery/jquery-2.1.0.min.js') ?>                    
    <?= $this->Html->script('bootstrap/bootstrap.min.js') ?>                        
    <?= $this->Html->script('plugins/modernizr/modernizr.js') ?>                   
    <?= $this->Html->script('plugins/bootstrap-tour/bootstrap-tour.custom.js') ?>  
    <?= $this->Html->script('king-common.js') ?>                                   
    <?= $this->Html->script('plugins/stat/jquery.easypiechart.min.js') ?>           
    <?= $this->Html->script('plugins/raphael/raphael-2.1.0.min.js') ?>              
    <?= $this->Html->script('plugins/stat/flot/jquery.flot.min.js') ?>             
    <?= $this->Html->script('plugins/stat/flot/jquery.flot.resize.min.js') ?>      
    <?= $this->Html->script('plugins/stat/flot/jquery.flot.time.min.js') ?>         
    <?= $this->Html->script('plugins/stat/flot/jquery.flot.pie.min.js') ?>         
    <?= $this->Html->script('plugins/stat/flot/jquery.flot.tooltip.min.js') ?>     
    <?= $this->Html->script('plugins/jquery-sparkline/jquery.sparkline.min.js') ?>  
    <?= $this->Html->script('plugins/datatable/jquery.dataTables.min.js') ?>       
    <?= $this->Html->script('plugins/datatable/dataTables.bootstrap.js') ?>        
    <?= $this->Html->script('plugins/jquery-mapael/jquery.mapael.js') ?>          
    <?= $this->Html->script('plugins/raphael/maps/usa_states.js') ?>               
    <?= $this->Html->script('king-chart-stat.js') ?>                               
    <?= $this->Html->script('king-table.js') ?>                                     
    <?= $this->Html->script('king-components.js') ?>                               
        
</body>

</html>
