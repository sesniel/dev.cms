<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<title><?php echo ucwords($this->request->params['action']); ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="KingAdmin Dashboard">
	<meta name="author" content="The Develovers">
	<!-- CSS -->
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('font-awesome.min.css') ?>
        <?= $this->Html->css('main.css') ?>
        
	<!--[if lte IE 9]>
            <?= $this->Html->css('main-ie.css') ?>
            <?= $this->Html->css('main-ie-part2.css') ?>
	<![endif]-->
	<!-- Fav and touch icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/kingadmin-favicon144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/kingadmin-favicon114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/kingadmin-favicon72x72.png">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/ico/kingadmin-favicon57x57.png">
	<link rel="shortcut icon" href="assets/ico/favicon.png">
</head>

<body>
    
<div class="wrapper full-page-wrapper page-auth page-login text-center">
        <div class="inner-page">
            
    
            <?= $this->fetch('content') ?>
    
        </div>
</div> 
            
	<footer class="footer">&copy; 2014-2015 The Develovers</footer>
    <!-- Javascript -->        
    <?= $this->Html->script('jquery/jquery-2.1.0.min.js') ?>
    <?= $this->Html->script('bootstrap/bootstrap.js') ?>
    <?= $this->Html->script('plugins/modernizr/modernizr.js') ?>
        
</body>

</html>
