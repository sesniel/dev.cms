<div class="container"> 
    <div class="row">
        <div class="col-md-12"><h1>Account Settings</h1></div>
    </div>
    <div class="row">        
        <div class="container well col-md-12">
            
            <div class="container">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>Name</th>
                            <td>
                                <?php
                                    if($userInfo['first_name'] != "" && $userInfo['last_name'] != ""):
                                        echo $userInfo['first_name'] . " " . $userInfo['last_name'] ;
                                    else:
                                        echo "Please fill in your name.";
                                    endif;
                                ?>
                            </td>
                            <td><?php echo $this->Html->link('Edit',array('controller' => 'settings', 'action' => 'name')); ?></td>
                        </tr>
                        <tr>
                            <th>Profile Picture</th>
                            <td>
                                <?php
                                    if($this->request->session()->read("image") != "None" ):
                                        echo '<img src="/img/profile_image/'.$userInfoData['image'].'" style="width:25px;height:25px;" alt="User Avatar" />';
                                    else:
                                        echo '<img src="/img/user-avatar.png" alt="User Avatar" />';
                                    endif;
                                ?>
                            </td>
                            <td><?php echo $this->Html->link('Edit',array('controller' => 'settings', 'action' => 'profileimage')); ?></td>
                        </tr>
                        <tr>
                            <th>Contact</th>
                            <td><?= $userInfo['email'] ?></td>
                            <td><?php echo $this->Html->link('Edit',array('controller' => 'settings', 'action' => 'contact')); ?></td>
                        </tr>
                        <tr>
                            <th>Password</th>
                            <td><?= $userInfo['password_last_update'] ?></td>
                            <td><?php echo $this->Html->link('Edit',array('controller' => 'settings', 'action' => 'password')); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>

    
</div>