<div class="container">
    <div class="row">
        <div class="col-md-12"><h1>Account Settings</h1></div>
    </div>
    <div class="row">
        <div class="col-md-12"><h3>Update <?php echo ucwords($this->request->params['action']); ?></h3></div>
    </div>
    <div class="row">
        <div class="col-md-12">
                <?php
                        echo $this->Flash->render();
                ?>
        </div>
    </div>
    
    
<?php     if($this->request->params['action'] == "name"): ?>
<?php 
echo $this->Form->create(null, [
        'url' => ['action' => 'savesettings']
    ]);
?>
    
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <table class="table table-striped borderless">
                <tr>
                    <th>First</th>
                    <td><input type="text" class="form-control" value="<?= $UserDetails["first_name"] ?>" name="first_name" required=""></td>
                </tr>
                <tr>
                    <th>Middle</th>
                    <td><input type="text" class="form-control" value="<?= $UserDetails["middle_name"] ?>" placeholder="Optional" name="middle_name"></td>
                </tr>
                <tr>
                    <th>Last</th>
                    <td><input type="text" class="form-control" value="<?= $UserDetails["last_name"] ?>" name="last_name" required=""></td>
                </tr>
                <tr><td colspan="2"><button type="submit" class="pull-right btn btn-primary">Update</button></td></tr>
            </table>
        </div>
        <div class="col-md-3"></div>
    </div>
<?php echo $this->Form->end();?>
    
<?php elseif($this->request->params['action'] == "profileimage"): ?>

<?php 
echo $this->Form->create(null, [
        'url' => ['action' => $this->request->params['action']],
        'enctype' => 'multipart/form-data'
    ]);
?>
    
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <table class="table table-striped borderless">
                <tr>
                    <td colspan="2">
                        <?php echo $this->Form->input('Images', ['type' => 'file', 'class' => 'form-control']); ?>
                    </td>
                </tr>
                <tr><td colspan="2"><button type="submit" class="pull-right btn btn-primary">Update</button></td></tr>
            </table>
        </div>
        <div class="col-md-3"></div>
    </div>
<?php echo $this->Form->end();?>
        
<?php elseif($this->request->params['action'] == "contact"): ?>

<?php 
echo $this->Form->create(null, [
        'url' => ['action' => 'savesettings']
    ]);
?>
    
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">  
            <table class="table table-striped borderless">
                <tr>
                    <th>Email</th>
                    <td><?= $UserDetails['email'] ?></td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <td><input type="text" class="form-control" value="<?= $UserDetails['phone'] ?>" placeholder="Optional" name="phone"></td>
                </tr>
                <tr><td colspan="2"><button type="submit" class="pull-right btn btn-primary">Update</button></td></tr>
            </table>
        </div>
        <div class="col-md-3"></div>
    </div>
<?php echo $this->Form->end();?>
    
<?php elseif($this->request->params['action'] == "password"): ?>
<?php 
echo $this->Form->create(null, [
        'url' => ['action' => $this->request->params['action']]
    ]);
?>
    
    <div class="row">
        <div class="col-md-12">Please note when successful, you will logout once you change your password details.<br/><br/></div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <table class="table table-striped borderless">
                <tr>
                    <th>Current Password</th>
                    <td><input type="password" class="form-control" name="current" required=""></td>
                </tr>
                <tr>
                    <th>New Password</th>
                    <td><input type="password" class="form-control" placeholder="password" name="password"></td>
                </tr>
                <tr>
                    <th>Confirm New Password</th>
                    <td><input type="password" class="form-control" name="confirm" required=""></td>
                </tr>
                <tr><td colspan="2"><button type="submit" class="pull-right btn btn-primary">Update</button></td></tr>
            </table>
        </div>
        <div class="col-md-3"></div>
    </div>
<?php echo $this->Form->end();?>
        
<?php endif; ?>

</div>

