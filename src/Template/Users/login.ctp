<div class="login-box center-block">
        <?php 
            echo $this->Form->create(null, [
            'url' => ['action' => 'login'],
            'type' => 'post', 'class' => 'form-horizontal']); 
        ?>
                <p class="title">Use your username</p>
                <div class="form-group">
                        <label for="username" class="control-label sr-only">Username</label>
                        <div class="col-sm-12">
                                <div class="input-group">
                                        <input type="email" placeholder="username" name="email" class="form-control">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                </div>
                        </div>
                </div>
                <label for="password" class="control-label sr-only">Password</label>
                <div class="form-group">
                        <div class="col-sm-12">
                                <div class="input-group">
                                        <input type="password" placeholder="password" name="password" class="form-control">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                </div>
                        </div>
                </div>
<!--                <label class="fancy-checkbox">
                        <input type="checkbox">
                        <span>Remember me next time</span>
                </label>-->
                <button class="btn btn-success btn-lg btn-block btn-auth"><i class="fa fa-arrow-circle-o-right"></i> Login</button>
        <?php echo $this->Form->end(); ?>
</div>
