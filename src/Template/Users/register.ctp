<div class="register-box center-block">

        <?php 
            echo $this->Form->create(null, [
            'url' => ['action' => 'register'],
            'type' => 'post', 'class' => 'form-horizontal']); 
        ?>
                <p class="title">Create Your Account</p>
                <input type="email"    name="email" placeholder="email" class="form-control">
                <input type="password" name="password" placeholder="password" class="form-control">
                <input type="password" name="confirm_password" placeholder="repeat password" class="form-control">
                <label class="fancy-checkbox">
                        <input type="checkbox" name="checkbox" required>
                        <span>I accept the <a href="#">Terms &amp; Agreements</a></span>
                </label>
                <button class="btn btn-custom-primary btn-lg btn-block btn-auth"><i class="fa fa-check-circle"></i> Create Account</button>
        <?php echo $this->Form->end(); ?>
</div>