<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Wager'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Jobs'), ['controller' => 'Jobs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Job'), ['controller' => 'Jobs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Entries'), ['controller' => 'Entries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Entry'), ['controller' => 'Entries', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="wagers index large-9 medium-8 columns content">
    <h3><?= __('Wagers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('job_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('draw_number') ?></th>
                <th><?= $this->Paginator->sort('draw_date') ?></th>
                <th><?= $this->Paginator->sort('ticket') ?></th>
                <th><?= $this->Paginator->sort('process_date') ?></th>
                <th><?= $this->Paginator->sort('fsn') ?></th>
                <th><?= $this->Paginator->sort('status') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($wagers as $wager): ?>
            <tr>
                <td><?= $this->Number->format($wager->id) ?></td>
                <td><?= $wager->has('job') ? $this->Html->link($wager->job->id, ['controller' => 'Jobs', 'action' => 'view', $wager->job->id]) : '' ?></td>
                <td><?= $wager->has('product') ? $this->Html->link($wager->product->name, ['controller' => 'Products', 'action' => 'view', $wager->product->id]) : '' ?></td>
                <td><?= h($wager->draw_number) ?></td>
                <td><?= h($wager->draw_date) ?></td>
                <td><?= h($wager->ticket) ?></td>
                <td><?= h($wager->process_date) ?></td>
                <td><?= h($wager->fsn) ?></td>
                <td><?= h($wager->status) ?></td>
                <td><?= h($wager->created) ?></td>
                <td><?= h($wager->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $wager->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $wager->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $wager->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wager->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
