<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Wager'), ['action' => 'edit', $wager->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Wager'), ['action' => 'delete', $wager->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wager->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Wagers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wager'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Jobs'), ['controller' => 'Jobs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Job'), ['controller' => 'Jobs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Entries'), ['controller' => 'Entries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Entry'), ['controller' => 'Entries', 'action' => 'add']) ?> </li>
    </ul>
</nav>-->
<div class="row">
    
    <div class="col-lg-6">
        <h1><?= $wager->draw_number ?></h1>
        Draw Date: <?= $wager->draw_date ?> <br/>
        Product: <?= $wager->has('product') ? $wager->product->name : '' ?> <br/>
        Status: <?= $wager->status ?>
    </div>
    
</div>
<div class="wagers view large-9 medium-8 columns content">
    
    <div class="related">
        <h4><?= __('Related Entries') ?></h4>
        <?php if (!empty($wager->entries)): ?>
        <table class="table" cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Detail Id') ?></th>
                <th><?= __('Lotto Number') ?></th>
            </tr>
            <?php foreach ($wager->entries as $entries): //debug($entries);?>
            <tr>
                <td><?= h($entries->detail_full) ?></td>
                <td>
                    <?php 
                        $ticketNumber = str_split($entries->details,2);
                        $num = count($ticketNumber);
                        $x = 1;
                        foreach($ticketNumber as $ticketD):
                            echo $ticketD;
                            if($x < $num):
                               echo " | "; $x++;
                            endif;
                        endforeach;
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
