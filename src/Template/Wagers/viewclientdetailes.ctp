<?php
    $wagerDate = array();
    $entries_count= array();
    foreach($wagers as $wagerDetail):
        $wagerDate[$wagerDetail->draw_date][] = $wagerDetail;
        $entries_count[$wagerDetail->draw_date] = "";
    endforeach;
//    echo count($wagerDate);
//    debug($wagerDate);
    foreach($wagerDate as $key => $wagers): 
        foreach($wagers as $wagerDetail):
            $entries_count[$key] += count($wagerDetail->entries);
        endforeach;
    endforeach;
?>

<div class="row">
    <div class="col-lg-12">
        <h3>Name: <small> <?= $user->first_name ?> <?= $user->last_name ?> </small></h3>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Product Name</th>
            <th>Wagers</th>
            <th>Entries</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($wagerDate as $wKey => $clientDetails): ?>
    
    
        <tr>
            <td>            
                <?php 
                    $day = date('l',strtotime($wKey) );  
                    $prodDisplay = "Not a valid Product";
                    foreach($products as $prod):
                        if($prod->day == $day):
                            $prodDisplay = $prod->name;
                        endif;
                    endforeach;
                    echo $prodDisplay;
                ?>
            </td>
            <td><?= count($clientDetails) ?></td>
            <td><?= $entries_count[$wKey] ?></td>
            <td>                    
                <?= $this->Html->link('<span class="text">Export</span>', 
                        ['controller' => 'jobs', 'action' => 'index'],['escape' => false, 'class' => 'btn btn-xs btn-primary']) 
                ?>              
                <?= $this->Html->link('<span class="text">Report</span>', 
                        ['controller' => 'jobs', 'action' => 'index'],['escape' => false, 'class' => 'btn btn-xs btn-primary']) 
                ?>              
                <?= $this->Html->link('<span class="text">Delete</span>', 
                        ['controller' => 'jobs', 'action' => 'index'],['escape' => false, 'class' => 'btn btn-xs btn-danger']) 
                ?>
            </td> 
        </tr>
    
    <?php endforeach; ?>
    </tbody>
    
</table>