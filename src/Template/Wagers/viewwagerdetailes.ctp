<?php
    
    $client_wager = array();
    $entries_count= array();
    foreach($wagers as $key => $wagerClientArrange):
        $client_wager[$wagerClientArrange->user->id][] = $wagerClientArrange;
        $entries_count[$wagerClientArrange->user->id] = "";
    endforeach;
    
    foreach($client_wager as $key => $wagers): 
        foreach($wagers as $wagerDetail):
            $entries_count[$key] += count($wagerDetail->entries);
        endforeach;
    endforeach;
    
?>

<div class="row">
    <div class="col-lg-12">
        <h3>Product: <small>
            <?php 
                $day = date('l',strtotime($date) );  
                $prodDisplay = "Not a valid Product";
                $prodDisplayId = "";
                foreach($products as $prod):
                    if($prod->day == $day):
                        $prodDisplay = $prod->name;
                        $prodDisplayId = $prod->id;
                    endif;
                endforeach;
                echo $prodDisplay;
            ?></small></h3>
        <h3>Date: <small><?= $date ?></small></h3>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Client Name</th>
            <th>Wager</th>
            <th>Entries</th>
            <th>Action</th>
        </tr>
    </thead>
    <?php foreach($client_wager as $cKey => $clientDetails): ?>
    
    <tbody>
        <tr>
            <td><?= $clientDetails[0]->user->first_name ?> <?= $clientDetails[0]->user->last_name ?></td>
            <td><?php echo count($clientDetails); ?></td>
            <td><?= $entries_count[$cKey] ?></td>
            <td>                    
                <?= $this->Html->link('<span class="text">Export</span>', 
                        ['controller' => 'wagers', 'action' => 'clientanddate',$clientDetails[0]->user->id, $date],['escape' => false, 'class' => 'btn btn-xs btn-primary']) 
                ?>              
                <?= $this->Html->link('<span class="text">Report</span>', 
                        ['controller' => 'jobs', 'action' => 'index'],['escape' => false, 'class' => 'btn btn-xs btn-primary']) 
                ?>              
                <?= $this->Html->link('<span class="text">Delete</span>', 
                        ['controller' => 'jobs', 'action' => 'index'],['escape' => false, 'class' => 'btn btn-xs btn-danger']) 
                ?>
            </td> 
        </tr>
    </tbody>
    <?php endforeach; ?>
</table>
