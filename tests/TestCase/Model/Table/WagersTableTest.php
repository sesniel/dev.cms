<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WagersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WagersTable Test Case
 */
class WagersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WagersTable
     */
    public $Wagers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.wagers',
        'app.jobs',
        'app.users',
        'app.products',
        'app.entries'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Wagers') ? [] : ['className' => 'App\Model\Table\WagersTable'];
        $this->Wagers = TableRegistry::get('Wagers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Wagers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
